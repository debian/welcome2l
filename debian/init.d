#!/bin/sh
# vim:ts=4:et:sts=4
### BEGIN INIT INFO
# Provides:          welcome2l
# Required-Start:    $local_fs $remote_fs
# Required-Stop:
# Should-Start:
# Should-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:
# Short-Description: Linux ANSI boot logo
# Description:       Write the Linux ANSI boot logo to the
#                    /etc/issue.welcome2l file
### END INIT INFO

PATH="/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin"
DAEMON="/usr/bin/welcome2l"
NAME="welcome2l"
DEFAULTSFILE="/etc/default/$NAME"

test -f "$DAEMON" || exit 0


# Read our defaults file
test -r "$DEFAULTSFILE" && . "$DEFAULTSFILE"

# Set default issue file
ISSUE_FILE="${ISSUE_FILE:-/etc/issue.welcome2l}"

# Define LSB log_* functions.
. /lib/lsb/init-functions

set -e
umask 022
status=0

case "$1" in
  start|restart|reload|force-reload)
    log_action_begin_msg "Updating $ISSUE_FILE"

    $DAEMON ${OPTIONS:--getty} \
            ${MSG:+-msg "$MSG"} \
            ${CPU:+-cpu "$CPU"} > "$ISSUE_FILE" || status=$?

    echo >> "$ISSUE_FILE"

    log_action_end_msg $status
    ;;

  stop)
    ;;

  *)
    log_failure_msg "Usage: /etc/init.d/$NAME {start|stop|restart|reload|force-reload}" 2>&1
    exit 1
    ;;
esac

exit 0
